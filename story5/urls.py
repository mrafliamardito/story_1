from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('result/', views.jadwal_ku,name = 'liat_jadwal'),
    path('<int:pk>/', views.delete, name = 'hapus'),
    path('<int:pk>', views.detail, name='detail'),
    # path('tes/', views.tes, name = 'tes'),
    
]